package com.goda.godasocialmediaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GodaSocialMediaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GodaSocialMediaServiceApplication.class, args);
	}
}
