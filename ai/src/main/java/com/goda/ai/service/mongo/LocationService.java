package com.goda.ai.service.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import com.goda.ai.model.mongo.Location;
import com.goda.ai.model.mongo.ReviewDto;
import com.goda.ai.repository.mongo.LocationRepository;

@Service
public class LocationService {
    @Autowired
    private LocationRepository locationRepository;

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public List<Location> findLocationsWithin(double longitude, double latitude, int distance) {
        return locationRepository.findByLocationNear(longitude, latitude, distance * 1000);
    }

    public List<Location> findLocationsWithinByType(double longitude, double latitude, int distance, String type) {
        return locationRepository.findByLocationNearByType(longitude, latitude, distance * 1000, type);
    }

    public Location findSpecifyLocation(String id){
        return locationRepository.findById(id).get();
    }
    public Location addReviewToLocation(String locationId, ReviewDto reviewDto) {
        Location location = locationRepository.findById(locationId).orElse(null);
        if (location != null) {
            Location.Review review = new Location.Review();
            review.setName(reviewDto.getName());
            review.setReviewTime(reviewDto.getReviewTime());
            review.setRating(reviewDto.getRating());
            review.setReviewContent(reviewDto.getReviewContent());
            
            location.getReviews().add(review);
            return locationRepository.save(location);
        }
        return null;
    } 
}
