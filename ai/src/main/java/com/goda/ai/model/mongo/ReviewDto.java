package com.goda.ai.model.mongo;

import lombok.Data;

@Data
public class ReviewDto {
    private String name;
    private String reviewTime;
    private String rating;
    private String reviewContent;
}