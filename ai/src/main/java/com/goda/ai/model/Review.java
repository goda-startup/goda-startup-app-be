package com.goda.ai.model;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Entity
@Data
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String textContent;
    private Date createdAt;
    private Date updatedAt;

    @OneToMany(mappedBy = "review", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Media> medias;

    @OneToMany(mappedBy = "review")
    private List<Comment> comments;

    @ManyToOne
    private User user;

    // @ManyToMany
    // @JoinTable(name = "review_users")
    // private List<User> referUsers;
    // private Place place;

    @ManyToMany
    @JoinTable(name = "likes_users")
    private List<User> likes;

    @JsonGetter("comments")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public List<Comment> getComments() {
        return comments.stream()
                .map(comment -> {
                    Comment returnComment = new Comment();
                    User author = comment.getAuthor();
                    User returnAuthor = new User();
                    returnAuthor.setUsername(author.getUsername());
                    returnAuthor.setAvatar(author.getAvatar());
                    returnComment.setAuthor(returnAuthor);
                    return returnComment;
                }).collect(Collectors.toList());
    }

    @JsonGetter("likes")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public List<User> getLikes() {
        return likes.stream()
                .map(user -> {
                    User result = new User();
                    result.setId(user.getId());
                    result.setUsername(user.getUsername());
                    return result;
                })
                .collect(Collectors.toList());
    }

}
