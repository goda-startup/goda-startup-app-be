package com.goda.ai.repository.mongo;

import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.repository.Query;
import java.util.List;

import com.goda.ai.model.mongo.Location;

@Repository
public interface LocationRepository extends MongoRepository<Location, String>{
    @Query(value = "{\n" + //
            "  \"location.coordinates\": {\n" + //
            "    $near: {\n" + //
            "      $geometry: {\n" + //
            "         type: \"Point\",\n" + //
            "         coordinates: [?0, ?1]\n" + //
            "      },\n" + //
            "      $maxDistance: ?2\n" + //
            "    }\n" + //
            "  }\n" + //
            "}", sort = "{'avg_rate': -1}")
    List<Location> findByLocationNear(double longitude, double latitude, int distance);

    @Query(value = "{\n" + //
            "  \"location.coordinates\": {\n" + //
            "    $near: {\n" + //
            "      $geometry: {\n" + //
            "         type: \"Point\",\n" + //
            "         coordinates: [?0, ?1]\n" + //
            "      },\n" + //
            "      $maxDistance: ?2\n" + //
            "    }\n" + //
            "  },\n" + //
            "  \"type\": ?3" +
            "}", sort = "{'avg_rate': -1}")
    List<Location> findByLocationNearByType(double longitude, double latitude, int distance, String type);
}
