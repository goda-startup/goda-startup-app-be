package com.goda.ai.controllers.mongo;

import com.goda.ai.model.mongo.Location;
import com.goda.ai.model.mongo.ReviewDto;
import com.goda.ai.service.mongo.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/api/reviews")
public class MongoReviewController {

    @Autowired
    private LocationService locationService;

    @PostMapping("/{locationId}")
    public ResponseEntity<String> addReviewToLocation(@PathVariable String locationId, @RequestBody ReviewDto reviewDto) {
        try {
            locationService.addReviewToLocation(locationId, reviewDto);

            return ResponseEntity.ok("add success");
        } catch (Exception e) {
            return ResponseEntity.ok("add error");
        }
    }
}
