package com.goda.ai.controllers.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.goda.ai.service.mongo.LocationService;
@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/api/location")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/near")
    public ResponseEntity<?> getAllLocationsWithDistance(
            @RequestParam double longitude,
            @RequestParam double latitude,
            @RequestParam int distance) throws RuntimeException {
        return ResponseEntity.ok(locationService.findLocationsWithin(longitude, latitude, distance));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/type")
    public ResponseEntity<?> getAllLocationsByType(
            @RequestParam double longitude,
            @RequestParam double latitude,
            @RequestParam int distance,
            @RequestParam String type) throws RuntimeException {
        return ResponseEntity.ok(locationService.findLocationsWithinByType(longitude, latitude, distance, type));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOneLocation(@PathVariable String id) {
        return ResponseEntity.ok(locationService.findSpecifyLocation(id));
    }
    
    @PreAuthorize("hasRole('USER')")
    @GetMapping("/all")
    public ResponseEntity<?> getAllLocation() {
        return ResponseEntity.ok(locationService.getAllLocations());
    }
}
