package com.goda.godarecommendationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GodaRecommendationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GodaRecommendationServiceApplication.class, args);
	}

}
